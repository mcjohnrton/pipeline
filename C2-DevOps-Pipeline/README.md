# Getting Started
In directory containing docker-compose

### **Make a directory to store jankins data (must be called jenkins or else change docker-compose.yaml accourdingly**)
$ mkdir jenkins

### **Run docker compose**
$ docker-compose up -d

### **You need a password the first time you use jenkins. To find this passsword use:**
$ docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword

### **To reach your containers, use the following urls:**
- Jenkins     =    localhost:8081
- Gogs        =    localhost:3000
- Sonarqube   =    localhost:9000




gogs credentials:
	- Username: gogs
	- Email: gogs@gogs.com
	- Password: gogs123
	
jenkins credentials:
	- Username: admin
	- Password: admin123
	
